﻿using System;
using System.Linq;
using System.Web.Mvc;

using VetShopDB2;
using MVCViewModels;
using MySnippets;

namespace VetShopMVC.Controllers {
    public class LoginController : Controller {

        VetShopDBEntities db = Dao.GetInstance().GetEntities();

        ///<remarks>
        /// Login in is bound to the LoginViewModel class
        /// -> no data is bound on from the server to client.
        /// -> only user inputs - LoginID and Password is returned. The actual password/ideas never leaves the server
        ///</remarks>
        public ActionResult Index() {
            LoginViewModel myLoginVm = new LoginViewModel();
            return View(myLoginVm);
        }

        ///<summary>
        ///Checks and validates userID and password against LoginID and LoginPassword in table AnimalOwners
        ///</summary>
        ///<remarks>
        ///User data is bound to the LoginViewModel 
        ///check whether the user input has valid state and 
        /// If login succedeed -> save the User (= animalOwner instance) the Session, and redirect to 'Home' page
        /// or else (login failed) -> redirect the user to 'LoginFailed' page
        ///</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginViewModel myLoginVm) {
            if (ModelState.IsValid) {
                AnimalOwner animalOwner = null;
                try {
                    // get user object against validation in database table (returns null if not found)
                    animalOwner = (from ao in db.AnimalOwners
                                   where ao.loginID == myLoginVm.LoginName &&
                                         ao.loginPassword == myLoginVm.LoginPassWord &&
                                         ao.isStillAClient &&
                                         ao.isWebAccessEnabled
                                   select ao).FirstOrDefault();
                } catch (Exception ex) {
                    // happens periodically. It appears, when login is attemted too quickly right after launch in debug mode only 
                    ("loginControoler: asp.net failure: " + ex.Message).ToDebugWithTime();
                    return RedirectToAction("LoginFailed");
                }

                if (animalOwner == null) { // login failed
                    Session[MyStatic.SessionUser] = null; // reset login session
                    return RedirectToAction("LoginFailed");
                } else { // login succeded
                    Session[MyStatic.SessionUser] = animalOwner; // set login sessions (which is user in class LoginFilter to established whether user is logged in
                    return RedirectToAction("index", "home");
                }
            }

            // Invalid state -> user is redirected  to login page
            return View();
        }

        public ActionResult LoginFailed() {
            return View();
        }
    }
}