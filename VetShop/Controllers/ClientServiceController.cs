﻿using System.Linq;
using System.Web.Mvc;
using VetShopDB2;
using MVCViewModels;

namespace VetShopMVC.Controllers {
    public class ClientServiceController : Controller {

        VetShopDBEntities db = Dao.GetInstance().GetEntities();

        /// <summary>
        /// Lists the services that the VetShop provides
        /// </summary>
        /// <returns></returns>
        public ActionResult Index() {
            var clientServices = from cs in db.ClientServices
                                 where cs.isEnabled
                                 select new ServiceViewModel {
                                     Name = cs.name,
                                     Duration = cs.durationMinutes,
                                     Price = cs.price,
                                     ServiceDescription = cs.serviceDescription
                                 };
            return View(clientServices);
        }

        public ActionResult Details(int id) {
            return View();
        }

    }
}
