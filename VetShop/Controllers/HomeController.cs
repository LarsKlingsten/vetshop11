﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VetShopDB2;
using MVCViewModels;

namespace VetShopMVC.Controllers {

    /// <summary>
    /// Front Page of VetShop
    /// </summary>

    public class HomeController : Controller {
        public ActionResult Index() {

            AnimalOwner animalOwner = (AnimalOwner)Session[MyStatic.SessionUser]; // gets the loggedIn User or alternatively null (used to display login or user at home page)
            if (animalOwner != null) {
                @ViewBag.user = animalOwner.name;
            }
            return View();
        }

        public ActionResult About() {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact() {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}