﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using MVCViewModels;

using VetShopDB2;
using MySnippets;

namespace VetShopMVC.Controllers {

    [LoginFilter] // ensures that a user is always Logged in:
    public class BookingController : Controller {

        Dao myDao = Dao.GetInstance();

        private VetShopDBEntities db = Dao.GetInstance().GetEntities();

        public ActionResult Index() {
            AnimalOwner animalOwner = (AnimalOwner)Session[MyStatic.SessionUser];

            var bookings = from b in db.Bookings
                           where b.Animal.AnimalOwner.ID == animalOwner.ID
                           orderby b.consultationStart
                           select new ForwardBookingViewModel {
                               ConsultationStart = (DateTime)b.consultationStart,
                               AnimalName = b.Animal.name,
                               AnimalOwnerName = b.Animal.AnimalOwner.name,
                               ID = b.ID
                           };
            return View(bookings);
        }

        #region Setup Booking ViewModel (inkl dropdowns)
        private BookingViewModel SetupBookingViewModel() {
            AnimalOwner animalOwner = (AnimalOwner)Session[MyStatic.SessionUser];

            BookingViewModel bookingViewModel = new BookingViewModel();

            // Dropdown Animals Get the Animals belong to the Owner - make it ready to a dropdown list
            var qryAnimals = (from a in db.Animals
                              where a.AnimalOwner.loginID == animalOwner.loginID &&
                              a.isAnimalAlive
                              select new MyDropDownList { ID = a.ID, Name = a.name }).ToList();
            bookingViewModel.Animals = qryAnimals;

            // Get the Animals belong to the Owner - make it ready to a dropdown list
            var qryAvailableServices = (from a in db.ClientServices
                                        where a.isEnabled
                                        select new MyDropDownList { ID = a.ID, Name = "$" + a.price + " " + a.name }).ToList();
            bookingViewModel.AvailableServices = qryAvailableServices;

            bookingViewModel.ConsultationStart = null;
            bookingViewModel.AnimalOwnerName = animalOwner.name;
            bookingViewModel.SelectedAnimalID = qryAnimals.FirstOrDefault().ID;
            bookingViewModel.dates = myDao.GetAvailableBookingSlot();

            return bookingViewModel;
        }
        #endregion

        public ActionResult Create() {
            BookingViewModel bookingViewModel = SetupBookingViewModel();
            return View(bookingViewModel);
        }

        [HttpPost]
        public string AddService(int bookingID, int serviceID) { // aka BookingLine
            ("prior add bookingLine count#" + db.BookingLines.Count()).ToDebugWithTime();
            ClientService service = db.ClientServices.Find(serviceID);
            BookingLine bookingLine = new BookingLine();
            bookingLine.fk_ClientService = serviceID;
            bookingLine.fk_Booking = bookingID;
            bookingLine.price = service.price;
            bookingLine.serviceText = service.name;
            db.BookingLines.Add(bookingLine);
            db.SaveChanges();
            return "ok: Added Service " + service.name + " to db";
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BookingViewModel bookingViewModelFromBrowser) {
            if (ModelState.IsValid) {

                Booking booking = new Booking();
                booking.fk_animal = (int)bookingViewModelFromBrowser.SelectedAnimalID;
                booking.updated = DateTime.Now;
                booking.consultationStart = bookingViewModelFromBrowser.ConsultationStart;

                // preselect the 1st booking from the ClientService table
                ClientService clientService = (from cs in db.ClientServices select cs).First();  

                BookingLine bookingLine = new BookingLine {
                                          fk_ClientService = clientService.ID,
                                          price = clientService.price,
                                          serviceText = clientService.name  };
                
                booking.BookingLines.Add(bookingLine);

                db.Bookings.Add(booking);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            // invalid state
            BookingViewModel bookingViewModel = SetupBookingViewModel();
            bookingViewModel.SelectedAnimalID = bookingViewModelFromBrowser.SelectedAnimalID; // maintain user imputs, if any
            bookingViewModel.ConsultationStart = bookingViewModelFromBrowser.ConsultationStart; // maintain user imputs, if any
            return View(bookingViewModel);
        }

        public ActionResult Edit(int id) {
            Booking booking = db.Bookings.Single(b => b.ID == id);
            BookingViewModel bookingViewModel = SetupBookingViewModel();
            bookingViewModel.ConsultationStart = booking.consultationStart;
            bookingViewModel.SelectedAnimalID = booking.fk_animal;
            bookingViewModel.PurchasedServices = GetPurchasedServicesList(id);
            bookingViewModel.ID = booking.ID;

            return View(bookingViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, BookingViewModel bookingViewModelFromBrowser) {

            Booking booking = db.Bookings.Find(id);
            if (ModelState.IsValid) {
                booking.fk_animal = (int)bookingViewModelFromBrowser.SelectedAnimalID;
                booking.updated = DateTime.Now;
                booking.consultationStart = bookingViewModelFromBrowser.ConsultationStart;
                db.Entry(booking).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            // invalid state - return user to the edit page
            BookingViewModel bookingViewModel = SetupBookingViewModel();
            bookingViewModel.SelectedAnimalID = bookingViewModelFromBrowser.SelectedAnimalID; // maintain user imputs, if any
            bookingViewModel.ConsultationStart = bookingViewModelFromBrowser.ConsultationStart; // maintain user imputs, if any
            return View(bookingViewModel);
        }

        #region delete bookings
        public ActionResult Delete(int id) {
            Booking booking = db.Bookings.Find(id);

            ForwardBookingViewModel bookingViewModel = new ForwardBookingViewModel {
                ID = id,
                ConsultationStart = (DateTime)booking.consultationStart,
                AnimalName = booking.Animal.name,
                AnimalType = booking.Animal.Specie.animalType
            };

            return View(bookingViewModel);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id) {

            Booking booking = db.Bookings.Find(id);
            db.Bookings.Remove(booking);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // no reason to reconfirm deletion (this is a service)
        public ActionResult DeleteService(int id) { // aka BookingLine

            BookingLine bookingLine = db.BookingLines.Find(id);

            try {
                db.BookingLines.Remove(bookingLine);
                db.SaveChanges();
            } catch (Exception e) {
                ("Error: BookingCtrl/DeleteService " + e.InnerException).ToDebugWithTime();
            }

            return RedirectToAction("edit/" + bookingLine.fk_Booking); // return the the same edit/(value)
        }

        #endregion

        public ActionResult ForwardBookings() {
            var dbBookings = from b in db.Bookings
                             orderby b.consultationStart
                             select b;

            List<ForwardBookingViewModel> bookings = new List<ForwardBookingViewModel>(); // create ViewModel for the bookings

            foreach (var b in dbBookings) {  // loop through each bookoing (from Entities)

                List<ServiceViewModel> services = new List<ServiceViewModel>(); // 
                DateTime serviceStart = (DateTime)b.consultationStart;
                int totalMinutes = 0; // used to offset starttime of each service

                foreach (var s in b.BookingLines) {  // loop through each service in each booking
                    ServiceViewModel service = new ServiceViewModel {
                        Name = s.ClientService.name,
                        StartTime = serviceStart,
                        Duration = s.ClientService.durationMinutes
                    };
                    totalMinutes += s.ClientService.durationMinutes;
                    serviceStart = serviceStart.AddMinutes(totalMinutes);
                    services.Add(service);
                }

                ForwardBookingViewModel booking = new ForwardBookingViewModel {
                    AnimalName = b.Animal.name,
                    AnimalOwnerName = b.Animal.AnimalOwner.name,
                    ConsultationStart = (DateTime)b.consultationStart,
                    AnimalType = b.Animal.Specie.animalType,
                    DurationForFullBooking = totalMinutes, // from above total minutes
                    ClientServices = services // from above loop
                };

                bookings.Add(booking);
            }
            return View(bookings);
        }

        public PartialViewResult Services(int id) {
            return PartialView(GetPurchasedServicesList(id));
        }

        public List<ServiceViewModel> GetPurchasedServicesList(int id) {
            var services = (from bl in db.BookingLines
                            where bl.fk_Booking == id
                            select new ServiceViewModel {
                                Duration = bl.ClientService.durationMinutes,
                                Name = bl.serviceText,
                                Price = (int)bl.price,
                                StartTime = (DateTime)bl.Booking.consultationStart,
                                ID = bl.fk_Booking,
                                BookingLineID = bl.ID

                            }).ToList();
            return services;
        }
    }
}