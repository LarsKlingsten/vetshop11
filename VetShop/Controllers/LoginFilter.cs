﻿using System.Web;
using System.Web.Mvc;
using MVCViewModels;

namespace VetShopMVC.Controllers {

    /// <summary>
    /// checks whether a user is logged, and if not, then 
    /// redirect the user to the login controllers (where login is possible)
    /// </summary>
    /// <remarks>
    /// The loginFilter class is utilized as an annotations "[LoginFilter]" on 
    /// the controllers that requires the user to be logged in.  
    ///</remarks>
    public class LoginFilter : ActionFilterAttribute {
        public override void OnActionExecuting(ActionExecutingContext filterContext) {

            if (HttpContext.Current.Session[MyStatic.SessionUser] == null) {
                filterContext.Result = new RedirectResult("~/login");
            }

            base.OnActionExecuting(filterContext);
        }
    }
}

