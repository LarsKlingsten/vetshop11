﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MVCViewModels {
    public class LoginViewModel {

        public int ID { get; set; }

        [DisplayName("Login ID")]
        [Required(ErrorMessage = "Please enter your login ServiceName")]
        public string LoginName { get; set; }

        [DisplayName("Login Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter your password")]
        public string LoginPassWord { get; set; }
    }

    public class ServiceViewModel {

        public int ID { get; set; }

        public int BookingLineID { get; set; }

        [DisplayName("Service")]
        public string Name { get; set; }

        [DisplayName("Description")]
        public string ServiceDescription { get; set; }

        [DisplayFormat(DataFormatString = "{0:#,###0.00}")]
        [DisplayName("Price $")]
        public int Price { get; set; }

        [DisplayName("Duration Minutes")]
        public int Duration { get; set; }

        public DateTime StartTime { get; set; }
    }

    public class ForwardBookingViewModel {

        public int ID { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        [DisplayName("Time")]
        public DateTime ConsultationStart { get; set; }

        [DisplayName("Duration")]
        public int DurationForFullBooking { get; set; }

        [DisplayName("Owner")]
        public string AnimalOwnerName { get; set; }

        [DisplayName("Animal Name")]
        public string AnimalName { get; set; }

        [DisplayName("Kind")]
        public string AnimalType { get; set; }

        public List<ServiceViewModel> ClientServices { get; set; }
    }


    public class BookingViewModel {

        public int ID { get; set; }

        [DisplayName("Owner")]
        [ReadOnly(true)]
        public string AnimalOwnerName { get; set; }

        [Required(ErrorMessage = "Please create an amimal")]
        public Nullable<int> SelectedAnimalID { get; set; }

        [DisplayName("Animal")]
        public List<MyDropDownList> Animals { get; set; }

        [Required(ErrorMessage = "Add Service")]
        public int SelectedServiceID { get; set; }

        [DisplayName("Add Service")]
        public List<MyDropDownList> AvailableServices { get; set; }

        [DisplayName("Consultatation Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please select a Date")]
        public Nullable<DateTime> ConsultationStart { get; set; }

        public DateTime[,] dates { get; set; }

        public List<ServiceViewModel> PurchasedServices { get; set; }

    }

    public class MyDropDownList {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}