﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MVCViewModels;

using VetShopDB2;
using MySnippets;
using System.Collections.Generic;

namespace UnitTest {
    [TestClass]
    public class UnitTest1 {

        Dao myDao = Dao.GetInstance();
        private VetShopDBEntities db = Dao.GetInstance().GetEntities();

        [TestMethod]
        public void TestAnimalOwnerInsertWithoutEnoughParameters() {

            AnimalOwner animalOwner = new AnimalOwner {name = "Lars Test" };
            List<EntityValidationErrors> errors = myDao.AddAnimalOwner(animalOwner);
            Assert.AreEqual(7, errors.Count);
        }

        [TestMethod]
        public void TestAnimalOwnerInsertWith() {
            var blockSlots = myDao.getBlockSlots();
            Assert.AreEqual(21, blockSlots.Count);
        }

    }
}
