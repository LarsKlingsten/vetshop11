﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySnippets;

namespace VetShopDB2 {
    public class Dao {
        #region class setup (singleton)

        private static readonly Dao instance = new Dao();
        private VetShopDBEntities db = new VetShopDBEntities();
        //private PopupWindowErrors popupWindowErrors;

        public static Dao GetInstance() { // eager initialization as always required, avoid pitsfall associated with singleton
            return instance;
        }
        #endregion class setup (singleton)

        public VetShopDBEntities GetEntities() {
            return db;
        }
         
        #region WPF CUD AnimalOwners
        public List<EntityValidationErrors> AddAnimalOwner(AnimalOwner animalOwner) {
            try {
                db.AnimalOwners.Add(animalOwner);
                db.SaveChanges();
            } catch (System.Data.Entity.Validation.DbEntityValidationException exceptionCollection) {
                return GetValidationErrors(exceptionCollection);
            }
            return null; //  == no errors -> all good;
        }

        public List<EntityValidationErrors> UpdateAnimalOwner(AnimalOwner animalOwner) {
            try {
                db.Entry(animalOwner).State = EntityState.Modified;
                db.SaveChanges();
            } catch (System.Data.Entity.Validation.DbEntityValidationException exceptionCollection) {
                return GetValidationErrors(exceptionCollection);
            }
            return null; //  == no errors -> all good;
        }

        //
        public List<EntityValidationErrors> DeleteAnimalOwner(AnimalOwner animalOwner) {
            try {
                db.AnimalOwners.Remove(animalOwner);
                db.SaveChanges();
            } catch (System.Data.Entity.Validation.DbEntityValidationException exceptionCollection) {
                return GetValidationErrors(exceptionCollection);
            }
            return null; //  == no errors -> all good;
        }

        #endregion CUD AnimalOwners

        #region WPF CUD ClientService
        public List<EntityValidationErrors> AddClientService(ClientService clientService) {
            try {
                db.ClientServices.Add(clientService);
                db.SaveChanges();
            } catch (System.Data.Entity.Validation.DbEntityValidationException exceptionCollection) {
                return GetValidationErrors(exceptionCollection);
            }
            return null; //  == no errors -> all good;
        }

        public List<EntityValidationErrors> UpdateClientService(ClientService clientService) {
            try {
                db.Entry(clientService).State = EntityState.Modified;
                db.SaveChanges();
            } catch (System.Data.Entity.Validation.DbEntityValidationException exceptionCollection) {
                return GetValidationErrors(exceptionCollection);
            }
            return null; //  == no errors -> all good;
        }

        //
        public List<EntityValidationErrors> DeleteClientService(ClientService clientService) {
            try {
                db.ClientServices.Remove(clientService);
                db.SaveChanges();
            } catch (System.Data.Entity.Validation.DbEntityValidationException exceptionCollection) {
                return GetValidationErrors(exceptionCollection);
            }
            return null; //  == no errors -> all good;
        }

        #endregion CUD Animal

        #region CRUD WPF Booking
        public List<EntityValidationErrors> AddBookingWPF(Booking booking) {
            try {
                db.Bookings.Add(booking);
                db.SaveChanges();
            } catch (System.Data.Entity.Validation.DbEntityValidationException exceptionCollection) {
                return GetValidationErrors(exceptionCollection);
            }
            return null; //  == no errors -> all good;
        }

        public List<EntityValidationErrors> UpdateBookingWPF(Booking booking) {
            try {
                db.Entry(booking).State = EntityState.Modified;
                db.SaveChanges();
            } catch (System.Data.Entity.Validation.DbEntityValidationException exceptionCollection) {
                return GetValidationErrors(exceptionCollection);
            }
            return null; //  == no errors -> all good;
        }

        //
        public List<EntityValidationErrors> DeleteBookingWPF(Booking booking) {
            try {
                db.Bookings.Remove(booking);
                db.SaveChanges();
            } catch (System.Data.Entity.Validation.DbEntityValidationException exceptionCollection) {
                return GetValidationErrors(exceptionCollection);
            }
            return null; //  == no errors -> all good;
        }

        public List<EntityValidationErrors> AddServiceToBooking(int clientServiceID, int bookingID) {
            try {
                ClientService clientService = db.ClientServices.Find(clientServiceID);
                BookingLine bookingLine = new BookingLine {
                    fk_Booking = bookingID,
                    fk_ClientService = clientServiceID,
                    price = (int)clientService.price,
                    serviceText = clientService.name
                };

                db.BookingLines.Add(bookingLine);
                db.SaveChanges();
            } catch (System.Data.Entity.Validation.DbEntityValidationException exceptionCollection) {
                return GetValidationErrors(exceptionCollection);
            }
            return null; //  == no errors -> all good;
        }

        public string RemoveServiceFromBooking(int clientServiceID, int bookingID) {
            try {
                ClientService clientService = db.ClientServices.Find(clientServiceID);
                var bookingLine = (from bl in db.BookingLines
                                   where bl.fk_ClientService == clientServiceID && bl.fk_Booking == bookingID
                                   select bl).First();
                db.BookingLines.Remove(bookingLine);
                db.SaveChanges();
            } catch {
                return " ups could not remove service";
            }
            return null; //  == no errors -> all good;
        }

         
        #endregion CRUD WPF Booking

        #region DB validation

        public List<EntityValidationErrors> GetValidationErrors(System.Data.Entity.Validation.DbEntityValidationException ex) {
            var errors = new List<EntityValidationErrors>();
            foreach (var eve in ex.EntityValidationErrors) {
                foreach (var ve in eve.ValidationErrors) {
                    errors.Add(new EntityValidationErrors {
                        // DatabaseTableName = eve.Entry.Entity.GetType().Name.ToString(),
                        // EntityState = eve.Entry.State.ToString(),
                        Field = ve.PropertyName.ToString(),
                        Message = ve.ErrorMessage.ToString()
                    });
                }
            }
            return errors;
        }


        //public void PopupWindowEntityValidationErrors(List<EntityValidationErrors> validationsErrors) {
        //    if (popupWindowErrors == null) {
        //        popupWindowErrors = new PopupWindowErrors();
        //    }
        //    popupWindowErrors.dataGridErrorList.ItemsSource = validationsErrors;
        //    popupWindowErrors.ShowDialog();
        //    popupWindowErrors.Focus();
        //}

        #endregion DB Validation

        #region available dates

        /// <summary>
        /// Uses the OpeningHour table
        /// 
        /// each row in OpeningHour contains the open booking slots for a particular hour/minute combination 
        /// 1) Hour (int)
        /// 2) Minute (int) 
        /// 3) openDayOfWeek of the Week (string)
        /// 
        /// Example:
        /// a row hour: 15, minute: 15,  openDayOfWeek: "12345"  
        /// means that the time slot 15:15 is available Monday, Tuesday, Wednesday, Thursdays, and Friday (not Sat or Sun) 
        /// 
        /// Example 2:
        /// a row hour: 15, minute: 45,  openDayOfWeek: "4"  
        /// means that the time slot 15:45 is available Thursdays only  
        /// 
        /// This allow for longer or shorter opening hour on certain dates
        ///  
        /// this is called SetupBookingViewModel() 
        /// </summary>
        /// <returns>a double array of DateTime where bookings are possible bookings</returns>


        // Get the block time slots 
        // -> by looping through the bookings
        // -> and their bookinglines
        // -> and the duration (dividing into slots of 15 miutes)
        // return a list of blocked time slots
        public  Dictionary<DateTime, int> getBlockSlots() {
            var blockedSlotsDict = new Dictionary<DateTime, int>();
            var bookings = from b in db.Bookings select b;
            foreach (var b in bookings) {
                DateTime blocked = (DateTime)b.consultationStart;
                foreach (var bl in b.BookingLines) {
                    for (int i = 0; i < bl.ClientService.durationMinutes / 15; i++) {
                        blockedSlotsDict.Add(blocked, bl.Booking.ID); // add the booking ID the blockedSlots 
                        blocked = blocked.AddMinutes(15);
                    }
                }
            }
            return blockedSlotsDict;
        }

       public DateTime[,] GetAvailableBookingSlot() {
            DateTime startDate = DateTime.Today;
            Dictionary<DateTime, int> blockedSlotsDict = getBlockSlots();

            int maxNumberOfDays = 10; // constant
            var openDates = from d in db.OpeningHours select d;

            DateTime[,] myDates = new DateTime[maxNumberOfDays, openDates.Count()];

            int daySlot = 0;
            while (daySlot < maxNumberOfDays) {

                // ship Saturdays and Sundays
                if (startDate.DayOfWeek == DayOfWeek.Saturday || startDate.DayOfWeek == DayOfWeek.Sunday) {
                    startDate = startDate.AddDays(1);
                    continue;
                }

                int timeSlot = 0;
                string dayOfTheWeek = ((int)startDate.DayOfWeek).ToString(); // i.e converts Tuesday -> 1 (int) -> "1" (string)

                foreach (var od in openDates) {
                    if (od.openDayOfWeek.Contains(dayOfTheWeek)) {
                        DateTime newDate = startDate.AddHours(od.hour).AddMinutes(od.minute);

                        // add open time slot unless is has been blocked (i.e. does not contain the key
                        if (!blockedSlotsDict.ContainsKey(newDate)) {
                            myDates[daySlot, timeSlot] = newDate;
                        }
                        timeSlot++;
                    }
                }
                daySlot++;

                startDate = startDate.AddDays(1);

            }
            return myDates;
        }

        #endregion








    }
}


