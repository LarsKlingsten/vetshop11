﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetShopDB2 {
    public class EntityValidationErrors {

          public string Field   { get; set; }
          public string Message  { get; set; }
  
    }
}
