﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySnippets {
 
    /// <summary>
    /// Extensions to 'string' class - that provide console/debug and add 'left' to the string method
    /// </summary>
    public static class MyExtensions {
        public static string Left(this string str, int stringLength) {
            if (stringLength <= 0) { return ""; }
            if (stringLength > str.Length) { stringLength = str.Length; }
            return str.Substring(0, stringLength);
        }

        public static void ToConsole(this string str) {
            Console.WriteLine(str);
        }

        public static void ToConsoleWithTime(this string str) {
            Console.WriteLine(System.DateTime.Now.ToString("hh:mm:ss.fff") + " " + str);
        }

        public static void ToDebugWithTime(this string str) {
            System.Diagnostics.Debug.WriteLine(System.DateTime.Now.ToString("hh:mm:ss.fff") + " " + str);
        }
    }
}
