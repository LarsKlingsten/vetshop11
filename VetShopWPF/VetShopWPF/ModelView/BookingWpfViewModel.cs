﻿using System;
using System.Collections.Generic;
using VetShopDB2;
using System.Windows.Controls;

namespace VetShopWPF.ModelView {
    public class BookingWpfViewModel {
        private readonly Booking booking;
        public int SeletedAnimal { get; set; }
        public List<int> SelectedServices { get; set; }
        public DateTime ConsultationStart { get; set; }
        public string Note { get; set; }
        private Button btnSave;

        public BookingWpfViewModel(Booking booking, Button btnSave) {
            this.booking = booking;
            this.btnSave = btnSave;

            if (booking.Animal != null) {
                this.SeletedAnimal = booking.Animal.ID;
                this.ConsultationStart = (DateTime)booking.consultationStart;
            }
        }
    }
}
