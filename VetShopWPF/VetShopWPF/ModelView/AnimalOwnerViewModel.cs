﻿using System;
using System.ComponentModel;
using VetShopDB2;
using System.Windows.Controls;
using MySnippets;

using System.Collections.Generic;

// Copyright Lars Klingsten 2015 //

namespace VetShopWPF.ModelView {
    public class AnimalOwnerViewModel : INotifyPropertyChanged, IDataErrorInfo {

        #region Setters/Getters
        private Dictionary<String, bool> textboxIsErrorStateDict;
        private Button btnSave;
        private readonly AnimalOwner animalOwner;
        public int ID { get; set; }
        public string Error { get; set; }
        public string OwnerName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string PostNumber { get; set; }
        public string ByNavn { get; set; }
        public string LoginID { get; set; }
        public string LoginPassword { get; set; }
        public bool IsStillAClient { get; set; }
        public bool IsWebAccessEnabled { get; set; }
        private string textBoxValueToBeEvaluated ="";
        
        #endregion  // Setters/Getters

        public AnimalOwnerViewModel(AnimalOwner animalOwner, Button btnSave) {
            textboxIsErrorStateDict = new Dictionary<String, bool>();
            this.animalOwner = animalOwner;
            this.btnSave = btnSave;
             
            OwnerName = animalOwner.name;
            Address = animalOwner.address;
            PhoneNumber = animalOwner.phoneNumber;
            EmailAddress = animalOwner.emailAddress;
            PostNumber = animalOwner.postNumber;
            ByNavn = animalOwner.byNavn;
            LoginID = animalOwner.loginID;
            LoginPassword = animalOwner.loginPassword;
            IsStillAClient = animalOwner.isStillAClient;
            IsWebAccessEnabled = animalOwner.isWebAccessEnabled;
        }

        public static object GetPropValue(object source, string propName) {
            return source.GetType().GetProperty(propName).GetValue(source, null);
        }

        public string this[string propertyName] {
            get {
 
                #region Get the values from the string property and save it for later usage
                try {
                    textBoxValueToBeEvaluated = (string)GetPropValue(this, propertyName);
                } catch {
                    return null; // do not evaluate non-string values (i.e. above cast to string fails)
                }
                #endregion Get the values from the string property and save it for later usage

                // disable the save button (will be enabled below if all values for each for the dictionary has errors
                btnSave.IsEnabled = false;

                // set the TextBox to IsError (will be corrected if validation is passed
                textboxIsErrorStateDict[propertyName] = true;

                #region validate textboxes -> exit method if validation fails
                string value;
                if (propertyName == "OwnerName") {
                    string errorMessage = ValidateTextBoxValue(false, 5, 50, out value);
                    if (!String.IsNullOrEmpty(errorMessage)) { return errorMessage; }
                } 
                
                else if (IsWebAccessEnabled && propertyName.Contains("Login")) {
                    string errorMessage = ValidateTextBoxValue(true, 5, 50, out value);
                    if (!String.IsNullOrEmpty(errorMessage)) { return errorMessage; }
                } else {
                    string errorMessage = ValidateTextBoxValue(true, 0, 50, out value);
                    if (!String.IsNullOrEmpty(errorMessage)) { return errorMessage; }
                }
                #endregion  

                // all good - validation has been passed // set IsError to false
                textboxIsErrorStateDict[propertyName] = false;

                #region Check whether all textbox are free from validation errors and if so Enable saveButton
                int countValidationErrors = 0;
                foreach (var errorState in textboxIsErrorStateDict.Values) {
                    if (errorState == true) { countValidationErrors++; }
                }
                if (countValidationErrors == 0) {
                    btnSave.IsEnabled = true;
                }

                #endregion Check whether all textbox are free from validation errors

                return " ";
            }
        }

      
        string ValidateTextBoxValue(bool nullValueAllowed, int minLength, int maxLength, out string value) {
            value = "";
            if (textBoxValueToBeEvaluated == null) { return null; }
            if (!nullValueAllowed && String.IsNullOrEmpty(textBoxValueToBeEvaluated)) { return "is required"; }
            if (textBoxValueToBeEvaluated.Length < minLength) { return "must be minimum " + minLength + " letters long"; }
            if (textBoxValueToBeEvaluated.Length > maxLength) { return "can't exceed " + maxLength + " letters"; }
            return null; // No Convertion Errors!
        }


        #region Notify the changed properties in
        public event PropertyChangedEventHandler PropertyChanged;
      
        void OnPropertyChanged(string propertyName) {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            propertyName.ToConsoleWithTime();
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}