﻿
namespace VetShopWPF.ModelView {
    
    enum EntryStatus {
        EntryNotSelected,
        EntryIsSelected,
        EntryIsEdited,
        EntryIsNew
    };

}
