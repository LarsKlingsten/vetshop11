﻿using System;
using System.ComponentModel;
using VetShopDB2;
using System.Windows.Controls;
using MySnippets;

using System.Collections.Generic;

// Copyright Lars Klingsten 2015 //

namespace VetShopWPF.ModelView {
    public class ClientServiceViewModel : INotifyPropertyChanged, IDataErrorInfo {

        #region Setters/Getters
        private Dictionary<String, bool> textboxIsErrorStateDict;
        private Button btnSave;
        private readonly ClientService clientService;
        public int ID { get; set; }
        public string Error { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public string ServiceDescription { get; set; }
        public string DurationMinutes { get; set; }
        public bool IsEnabled { get; set; }

        private string textBoxValueToBeEvaluated = "";

        #endregion  // Setters/Getters

        public ClientServiceViewModel(ClientService clientService, Button btnSave) {
            textboxIsErrorStateDict = new Dictionary<String, bool>();
            this.clientService = clientService;
            this.btnSave = btnSave;
            Name = clientService.name;
            Price = clientService.price.ToString();
            ServiceDescription = clientService.serviceDescription;
            DurationMinutes = clientService.durationMinutes.ToString();
            IsEnabled = clientService.isEnabled;
        }

        public static object GetPropValue(object source, string propName) {
            return source.GetType().GetProperty(propName).GetValue(source, null);
        }

        public string this[string propertyName] {
            get {

                #region Get the values from the string property and save it for later usage
                try {
                    textBoxValueToBeEvaluated = (string)GetPropValue(this, propertyName);
                } catch {
                    return null; // do not evaluate non-string values (i.e. above cast to string fails)
                }
                #endregion Get the values from the string property and save it for later usage

                // disable the save button (will be enabled below if all values for each for the dictionary has errors
                btnSave.IsEnabled = false;

                // set the TextBox to IsError (will be corrected if validation is passed
                textboxIsErrorStateDict[propertyName] = true;

                #region validate textboxes -> exit method if validation fails
                string value;
                int myNumber;
                string errorMessage = null;
                if (propertyName == "Name") {
                    errorMessage = ValidateTextBoxValue(false, 5, 50, out value);
                    if (!String.IsNullOrEmpty(errorMessage)) { return errorMessage; }
                }

                // check length of Service Description i between 5-200
                else if (propertyName == "ServiceDescription") {
                    errorMessage = ValidateTextBoxValue(true, 5, 200, out value);
                    if (!String.IsNullOrEmpty(errorMessage)) { return errorMessage; }
                }
                    // check value of price is between 0-100000
                else if (propertyName == "Price") {
                    errorMessage = ValidateInteger(0, 99999, out myNumber);
                    if (!String.IsNullOrEmpty(errorMessage)) { return errorMessage; }
                }
                    // check value of duration if between 0-60 - and is dividable by 15
                else if (propertyName == "DurationMinutes") {

                    errorMessage = ValidateInteger(0, 60, out myNumber);
                    if (!String.IsNullOrEmpty(errorMessage)) { return errorMessage; }

                    // validate whether dividable by 15
                    errorMessage = ValidateIntegerDivision(15, out myNumber);
                    if (!String.IsNullOrEmpty(errorMessage)) { return errorMessage; }
                }

                #endregion

                // all good - validation has been passed // set IsError to false
                textboxIsErrorStateDict[propertyName] = false;

                #region Check whether all textbox are free from validation errors and if so Enable saveButton
                int countValidationErrors = 0;
                foreach (var errorState in textboxIsErrorStateDict.Values) {
                    if (errorState == true) { countValidationErrors++; }
                }
                if (countValidationErrors == 0) {
                    btnSave.IsEnabled = true;
                }

                #endregion

                return " ";
            }
        }


        #region valuations

        string ValidateTextBoxValue(bool nullValueAllowed, int minLength, int maxLength, out string value) {
            value = "";
            if (textBoxValueToBeEvaluated == null) { return null; }
            if (!nullValueAllowed && String.IsNullOrEmpty(textBoxValueToBeEvaluated)) { return "is required"; }
            if (textBoxValueToBeEvaluated.Length < minLength) { return "must be minimum " + minLength + " letters long"; }
            if (textBoxValueToBeEvaluated.Length > maxLength) { return "can't exceed " + maxLength + " letters"; }
            return null; // No Convertion Errors!
        }

        string ValidateInteger(int minValue, int maxValue, out int myNumber) {
            myNumber = -1;
            if (String.IsNullOrEmpty(textBoxValueToBeEvaluated)) { return "is required"; }
            if (!Int32.TryParse(textBoxValueToBeEvaluated, out myNumber)) { return " is not a whole number."; }

            myNumber = Convert.ToInt32(textBoxValueToBeEvaluated);
            if (myNumber > maxValue) { return "can't be higher than " + maxValue; }
            if (myNumber < minValue) { return " can't be lower than " + minValue; }

            return null; // No Convertion Errors!
        }

        string ValidateIntegerDivision(int divBy, out int myNumber) {
            myNumber = -1;
            if (String.IsNullOrEmpty(textBoxValueToBeEvaluated)) { return "is required"; }
            if (!Int32.TryParse(textBoxValueToBeEvaluated, out myNumber)) { return " is not a whole number."; }

            myNumber = Convert.ToInt32(textBoxValueToBeEvaluated);
            if (myNumber % divBy != 0) { return "Must be Dividable with " + divBy; }

            return null; // No Convertion Errors!
        }

        #endregion


        #region Notify the changed properties in
        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string propertyName) {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            propertyName.ToConsoleWithTime();
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}