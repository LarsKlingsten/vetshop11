﻿using System;

namespace VetShopWPF.ModelView {
    public class BookingViewModel {
        public int ID { get; set; }
        public DateTime ConsultationStart { get; set; }
        public string Owner { get; set; }
        public string Animal { get; set; }
        public string Kind { get; set; }
    }

    public class EntityValidationErrors {
        public string Field { get; set; }
        public string Message { get; set; }
    }
 }