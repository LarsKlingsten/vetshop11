﻿
namespace VetShopWPF.ModelView {
    class ClientServiceListViewModel {

        public string Name { get; set; }
        public int Price { get; set; }
        public int DurationInMinutes { get; set; }
    }
}
