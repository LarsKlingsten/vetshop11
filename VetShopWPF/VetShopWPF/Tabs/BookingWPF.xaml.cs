﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Windows.Threading;

using VetShopDB2;
using VetShopWPF.ModelView;

namespace VetShopWPF.Tabs {

    /// <summary>
    /// Tab/windows that allows for CRUD operations on Booking Table (and associated tables)
    /// </summary>
    public partial class BookingWPF : UserControl {

        ObservableCollection<ClientServiceListViewModel> selectedServices;

        private Dao myDao = Dao.GetInstance();
        private VetShopDBEntities db = Dao.GetInstance().GetEntities();
        private BookingWpfViewModel bookingWpfViewModel;
        private Booking booking;
        private EntryStatus entryStatus;
        private List<TextBox> textBoxes;
        private List<CheckBox> checkBoxes;

        public BookingWPF() {
            InitializeComponent();
            textBoxes = GetAllTextBoxes();
            checkBoxes = GetAllCheckBoxes();
            ChangeEntryNotSelected();
            UpdateCbAnimals();
            UpdateCbBookings();

            selectedServices = new ObservableCollection<ClientServiceListViewModel>();
            dataGridBookings.ItemsSource = selectedServices;

            this.DataContext = bookingWpfViewModel;
        }

        private Booking ConvertViewModelToDbModel() {

            if (entryStatus == EntryStatus.EntryIsNew) { // for new entries 
                booking = new Booking();
            }
            booking.consultationStart = bookingWpfViewModel.ConsultationStart;
            return booking;
        }

        #region Clicks to Update, Create or Delete Booking
        private void btnSave_Click(object sender, RoutedEventArgs e) {
            Booking booking = ConvertViewModelToDbModel();

            if (entryStatus == EntryStatus.EntryIsNew) {
                var validationsErrors = myDao.AddBookingWPF(booking);

                if (validationsErrors != null) { // ERRORS EXIT (DATA WAS NOT COMMITTED TO DB)!
                    MsgToUser("Error. Failed Created New Record", 3);
                } else {
                    MsgToUser("OK. Created New Record", 3);
                }
            }

            if (entryStatus == EntryStatus.EntryIsEdited) {
                var validationsErrors = myDao.UpdateBookingWPF(booking);

                if (validationsErrors != null) { // ERRORS EXIT (DATA WAS NOT COMMITTED TO DB)!
                    MsgToUser("Error. Failed Update Record", 3);
                } else {
                    MsgToUser("OK. Updated Record", 3);
                }
            }
            UpdateCbAddService();
            UpdateCbRemoveService();
            ChangeEntryIsSelected();
        }
        #endregion

        #region Set ComboBoxes
        private void UpdateCbAddService() {
            var clientServices = (from b in db.ClientServices
                                  orderby b.name
                                  select new { Name = b.name, ID = b.ID })
                            .ToList();

            cbAddService.ItemsSource = clientServices;
            cbAddService.DisplayMemberPath = "Name";
            cbAddService.SelectedValuePath = "ID";
            cbAddService.SelectedValue = clientServices.FirstOrDefault();
        }

        private void UpdateCbRemoveService() {
            var clientServices = (from b in db.BookingLines
                                  where b.fk_Booking == booking.ID
                                  orderby b.ClientService.name
                                  select new { Name = b.ClientService.name, ID = b.fk_ClientService })
                            .ToList();

            if (clientServices.Count() > 1) {
                cbRemoveService.ItemsSource = clientServices;
                cbRemoveService.DisplayMemberPath = "Name";
                cbRemoveService.SelectedValuePath = "ID";
                cbRemoveService.SelectedValue = clientServices.FirstOrDefault();
            }
        }

        private void UpdateCbAnimals() {
            var animals = (from b in db.Animals
                           orderby b.name
                           select new { Name = b.name + " (" + b.AnimalOwner.name + ")", ID = b.ID })
                            .ToList();

            cbAnimals.ItemsSource = animals;
            cbAnimals.DisplayMemberPath = "Name";
            cbAnimals.SelectedValuePath = "ID";
            cbAnimals.SelectedValue = animals.FirstOrDefault();
        }

        private void UpdateCbBookings() {
            var animals = (from b in db.Bookings
                           orderby b.updated
                           select new { Name = b.Animal.name + " (" + b.Animal.AnimalOwner.name + ")", ID = b.ID })
                            .ToList();

            cbBookings.ItemsSource = animals;
            cbBookings.DisplayMemberPath = "Name";
            cbBookings.SelectedValuePath = "ID";
            cbBookings.SelectedValue = animals.FirstOrDefault();
        }
        #endregion ComboBoxes

        #region enable/disable buttons (plus init's)
        private void ChangeEntryNotSelected() {
            entryStatus = EntryStatus.EntryNotSelected;
            btnCancel.IsEnabled = false;
            btnEdit.IsEnabled = false;
            btnNew.IsEnabled = true;
            btnSave.IsEnabled = false;
            btnDelete.IsEnabled = false;
            cbAddService.IsEnabled = true;
            btnAddService.IsEnabled = false;
            cbAnimals.IsEnabled = false;
            cbAddService.IsEnabled = false;
            cbRemoveService.IsEnabled = false;
            cbBookings.IsEnabled = true;
            btnAddService.IsEnabled = false;
            btnRemoveService.IsEnabled = false;
            btnSelectDate.IsEnabled = false;
            EnableDisableUserInputConstrols(false);
        }

        private void ChangeEntryIsSelected() {

            entryStatus = EntryStatus.EntryIsSelected;
            btnCancel.IsEnabled = false;
            btnEdit.IsEnabled = true;
            btnNew.IsEnabled = true;
            btnSave.IsEnabled = false;
            btnDelete.IsEnabled = true;
            cbAddService.IsEnabled = false;
            cbRemoveService.IsEnabled = false;
            btnAddService.IsEnabled = false;
            btnRemoveService.IsEnabled = false;
            cbAnimals.IsEnabled = false;
            cbAddService.IsEnabled = false;
            btnSelectDate.IsEnabled = false;
            cbBookings.IsEnabled = true;
            EnableDisableUserInputConstrols(false);
        }

        private void ChangeEntryToEditMode() {
            MsgToUser("Edit Mode", 1);
            entryStatus = EntryStatus.EntryIsEdited;
            btnCancel.IsEnabled = true;
            btnEdit.IsEnabled = false;
            btnNew.IsEnabled = false;
            btnSave.IsEnabled = true;
            btnDelete.IsEnabled = false;
            btnAddService.IsEnabled = true;
            cbRemoveService.IsEnabled = true;
            btnRemoveService.IsEnabled = true;
            btnSelectDate.IsEnabled = true;
            cbAnimals.IsEnabled = true;
            cbAddService.IsEnabled = true;
            cbBookings.IsEnabled = false;
            EnableDisableUserInputConstrols(true);
        }

        private void ChangeEntryToNewMode() {
            MsgToUser("Creating New Entry", 1);
            entryStatus = EntryStatus.EntryIsNew;
            btnCancel.IsEnabled = true;
            btnEdit.IsEnabled = false;
            btnNew.IsEnabled = false;
            btnSave.IsEnabled = true;
            btnDelete.IsEnabled = false;
            btnAddService.IsEnabled = true;
            cbRemoveService.IsEnabled = true;
            btnRemoveService.IsEnabled = true;
            btnSelectDate.IsEnabled = true;
            cbAnimals.IsEnabled = true;
            cbAddService.IsEnabled = true;
            cbBookings.IsEnabled = false;
            EnableDisableUserInputConstrols(true);
        }

        private List<TextBox> GetAllTextBoxes() {
            var resultList = new List<TextBox>();
            foreach (var c in LogicalTreeHelper.GetChildren(GridClient)) {
                if (c.GetType() == typeof(TextBox)) {
                    resultList.Add((TextBox)c);
                }
            }
            return resultList;
        }

        private List<CheckBox> GetAllCheckBoxes() {
            var resultList = new List<CheckBox>();
            foreach (var c in LogicalTreeHelper.GetChildren(GridClient)) {
                if (c.GetType() == typeof(CheckBox)) {
                    resultList.Add((CheckBox)c);
                }
            }
            return resultList;
        }

        private void EnableDisableUserInputConstrols(bool enable) {
            foreach (var textBox in textBoxes) {
                textBox.IsEnabled = enable;
            }
            foreach (var checkBox in checkBoxes) {
                checkBox.IsEnabled = enable;
            }
        }

        #endregion

        private void btnEdit_Click(object sender, RoutedEventArgs e) {
            ChangeEntryToEditMode();
            UpdateCbAddService();
            UpdateCbRemoveService();
        }

        private void btnNew_Click(object sender, RoutedEventArgs e) {
            ChangeEntryToNewMode();

            // open new windows to to allow user to select a date
            SelectAnimalForBooking selectedAnimalForbooking = new SelectAnimalForBooking();
            selectedAnimalForbooking.Width = 300;
            selectedAnimalForbooking.Height = 300;
            selectedAnimalForbooking.ShowDialog();

            // ensure that the user has selected an animal, if not do nothing
            if (selectedAnimalForbooking.SelectedAnimalID != null) {
                int selectedAnimalID = (int)selectedAnimalForbooking.SelectedAnimalID;
                Animal animal = db.Animals.Find(selectedAnimalID);

                Booking booking = new Booking();
                booking.consultationStart = DateTime.Now.Date;
                booking.updated = DateTime.Now;
                booking.Animal = animal;

                // preselect the 1st booking from the ClientService table
                ClientService clientService = (from cs in db.ClientServices select cs).First();

                BookingLine bookingLine = new BookingLine {
                    fk_ClientService = clientService.ID,
                    price = clientService.price,
                    serviceText = clientService.name
                };

                booking.BookingLines.Add(bookingLine);


                db.Bookings.Add(booking);
                db.SaveChanges();
                UpdateCbBookings();

                var bookings = (from b in db.Bookings
                                where b.ID == booking.ID
                                orderby b.updated
                                select new { Name = b.Animal.name + " (" + b.Animal.AnimalOwner.name + ")", ID = b.ID })
                            .ToList();

                cbBookings.ItemsSource = bookings;
                cbBookings.DisplayMemberPath = "Name";
                cbBookings.SelectedValuePath = "ID";

                cbBookings.SelectedIndex = 0;
                UpdateCbBookings();
                ChangeEntryToEditMode();
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e) {
            MessageBoxResult result = MessageBox.Show(
                "Please confirm to delete " + booking.Animal.name + " on " + booking.consultationStart, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes) {
                ChangeEntryNotSelected();
                MsgToUser("Permanently Deleted Record", 5);
                var validationsErrors = myDao.DeleteBookingWPF(booking);
                Reset();

                if (validationsErrors != null) { // ERRORS EXIT (DATA WAS NOT COMMITTED TO DB)!
                    MsgToUser("Failed to Deleted Record", 5);
                }
            }

            ChangeEntryNotSelected();
        }

        private void Reset() {
            bookingWpfViewModel = null;
            this.DataContext = bookingWpfViewModel;
            ChangeEntryNotSelected();
            UpdateCbAddService();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) {
            MessageBoxResult result = MessageBox.Show("Do you wish to Cancel? Changes will be lost! ", "Cancel?", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes) {
                Reset();
                MsgToUser("Cancelled!", 5);
            }
        }

        #region UserMessage
        private void MsgToUser(string message, int seconds) {
            lblMsgToUser.Background = System.Windows.Media.Brushes.Yellow;
            lblMsgToUser.Content = message;
            initTimer(seconds);
        }

        private DispatcherTimer updateTimer;
        private void initTimer(int seconds) {
            updateTimer = new DispatcherTimer(DispatcherPriority.Normal);
            updateTimer.Tick += new EventHandler(OnUpdateTimerTick);
            updateTimer.Interval = TimeSpan.FromSeconds(seconds);
            updateTimer.Start();
        }

        private void OnUpdateTimerTick(object sender, EventArgs e) {
            lblMsgToUser.Background = System.Windows.Media.Brushes.Transparent;
            lblMsgToUser.Content = " ";
            updateTimer.Stop();
        }
        #endregion UserMessage

        private void cbBookings_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            selectedBooking();
        }

        private void selectedBooking() {
            ChangeEntryIsSelected();
            int bookingID = (int)cbBookings.SelectedValue;
            updateClientServiceGrid(bookingID);
            booking = (from ao in db.Bookings
                       where ao.ID == bookingID
                       select ao).Single();
            bookingWpfViewModel = new BookingWpfViewModel(booking, btnSave);
            this.DataContext = bookingWpfViewModel;
            cbAnimals.SelectedValue = booking.fk_animal;
        }


        private void cbAnimals_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            UpdateCbAddService();
            UpdateCbRemoveService();
        }

        #region Add Service To Booking
        private void btnAddService_Click(object sender, RoutedEventArgs e) {

            if (cbAddService.SelectedValue == null) { return; }

            int serviceID = (int)cbAddService.SelectedValue;
            myDao.AddServiceToBooking(serviceID, booking.ID);
            updateClientServiceGrid(booking.ID);
            UpdateCbAddService();
            UpdateCbRemoveService();

        }

        private void btnRemoveService_Click(object sender, RoutedEventArgs e) {

            if (cbRemoveService.SelectedValue == null) { return; }

            int serviceID = (int)cbRemoveService.SelectedValue;
            myDao.RemoveServiceFromBooking(serviceID, booking.ID);
            updateClientServiceGrid(booking.ID);
            UpdateCbAddService();
            UpdateCbRemoveService();


        }
        #endregion

        private void updateClientServiceGrid(int bookingID) {

            var clientServicesListViewModel = (from bl in db.BookingLines
                                               where bl.fk_Booking == bookingID
                                               select new ClientServiceListViewModel {
                                                   Name = bl.ClientService.name,
                                                   Price = (int)bl.price,
                                                   DurationInMinutes = bl.ClientService.durationMinutes,
                                               }).ToList();

            selectedServices.Clear();
            foreach (var b in clientServicesListViewModel) {
                selectedServices.Add(b);
            }
        }

        private void btnSelectDate_Click(object sender, RoutedEventArgs e) {
            SelectDateWindow selectedDateWindow = new SelectDateWindow();
            selectedDateWindow.ShowDialog();

            // ensure that a date has been selected !
            if (selectedDateWindow.selectedDate != null) {
                tbConsultationStart.Text = selectedDateWindow.selectedDate.ToLongTimeString();
            }
        }

    }
}
