﻿using System;
using System.Windows;
using System.Windows.Controls;

using VetShopDB2;

namespace VetShopWPF.Tabs {

    public partial class SelectDateWindow : Window {

        private Dao myDao = Dao.GetInstance();
        private VetShopDBEntities db = Dao.GetInstance().GetEntities();
        public DateTime selectedDate;

        public SelectDateWindow() {
            InitializeComponent();
            BuildButtons();
        }

        private void BuildButtons() {

            DateTime[,] AvailableSlots = myDao.GetAvailableBookingSlot();
            int maxColumn = 10;
            int offSetToAllowHeaderLabels = 1;

            int maxRows = AvailableSlots.GetLength(1);

            // create columns
            for (int c = 0; c < maxColumn ; c++) {
                ColumnDefinition newColumn = new ColumnDefinition();
                grid.ColumnDefinitions.Add(newColumn);
            }

            // create rows
            for (int r = 0 + offSetToAllowHeaderLabels; r < maxRows + offSetToAllowHeaderLabels; r++) {
                RowDefinition newRow = new RowDefinition();
                grid.RowDefinitions.Add(newRow);
            }


            for (int day = 0; day < maxColumn; day++) {
                Label myLabel = new Label();
                myLabel.Content = AvailableSlots[day, 0].ToString("dd/MM/yy");
                myLabel.FontWeight = FontWeights.Bold;
                Grid.SetRow(myLabel, 0);
                Grid.SetColumn(myLabel, day);
                grid.Children.Add(myLabel);
 
            }

            for (int timeSlot = 0; timeSlot < AvailableSlots.GetLength(1); timeSlot++) {
                for (int day = 0; day < maxColumn; day++) {
                    if (AvailableSlots[day, timeSlot].Year > 2012) {
 
                        Button myButton = new Button();
                        myButton.Content = AvailableSlots[day, timeSlot].ToString("HH:mm");
                        myButton.Name = AvailableSlots[day, timeSlot].ToString("DyyyyMMddHHmm"); // add 'D' to make it a valid button Name/Id
                        myButton.AddHandler(Button.ClickEvent, new RoutedEventHandler(button1_Click));

                        Grid.SetRow(myButton, timeSlot + offSetToAllowHeaderLabels);
                        Grid.SetColumn(myButton, day);
                         grid.Children.Add(myButton);
                    } else {
                        // create empty button or skip it
                    }
                }
            }
        }

        void button1_Click(object sender, RoutedEventArgs e) {
            Button button = (Button)sender;
            string myDateStr = button.Name.Substring(1, button.Name.Length-1); // get rid of the "d" set above to create a valid button id
            selectedDate = DateTime.ParseExact(myDateStr, "yyyyMMddHHmm", System.Globalization.CultureInfo.InvariantCulture);
            this.Close();
        }
    }
}