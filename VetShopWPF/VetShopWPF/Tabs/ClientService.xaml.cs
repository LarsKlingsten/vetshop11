﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;

using VetShopDB2;
using VetShopWPF.ModelView;

using System.Collections.Generic;
using System.Windows.Threading;
using System;

namespace VetShopWPF.Tabs {

    

    /// <summary>
    /// Tab/windows that allows for CRUD operations on Cli table
    /// </summary>
    public partial class ClientServiceTab : UserControl {

     

        private Dao myService = Dao.GetInstance();
        private VetShopDBEntities db = Dao.GetInstance().GetEntities();
        private ClientServiceViewModel clientServiceViewModel;
        private ClientService clientService;
        private EntryStatus entryStatus;
        private List<TextBox> textBoxes;
        private List<CheckBox> checkBoxes;

        public ClientServiceTab() {
            InitializeComponent();
            textBoxes = GetAllTextBoxes();
            checkBoxes = GetAllCheckBoxes();
            ChangeEntryNotSelected();
            SetClientServiceForComboBox();
            this.DataContext = clientServiceViewModel;
        }

        private ClientService ConvertClientServiceViewModelToClientService() {

            if (entryStatus == EntryStatus.EntryIsNew) { // for new entries 
                clientService = new ClientService();
            }

            clientService.name = clientServiceViewModel.Name;
            clientService.price = Convert.ToInt32(clientServiceViewModel.Price);
            clientService.serviceDescription = clientServiceViewModel.ServiceDescription;
            clientService.durationMinutes = Convert.ToInt32(clientServiceViewModel.DurationMinutes);
            clientService.isEnabled = clientServiceViewModel.IsEnabled;

            return clientService;
        }

        // Save the object
        private void btnSave_Click(object sender, RoutedEventArgs e) {
            ClientService clientService = ConvertClientServiceViewModelToClientService();

            // popup window for errors 
            //- which should only incur on database being unavailable
            // considering that each of the textboxes are validated.

            if (entryStatus == EntryStatus.EntryIsNew) {
                var validationsErrors = myService.AddClientService(clientService);


                if (validationsErrors != null) { // ERRORS EXIT (DATA WAS NOT COMMITTED TO DB)!
                    MsgToUser("Failed Created New Record", 3);
                } else {
                    MsgToUser("Created New Record", 3);
                }
            }

            if (entryStatus == EntryStatus.EntryIsEdited) {
                var validationsErrors = myService.UpdateClientService(clientService);

                if (validationsErrors != null) { // ERRORS EXIT (DATA WAS NOT COMMITTED TO DB)!
                    MsgToUser("Failed Update Record", 3);
                } else {
                    MsgToUser("Updated Record", 3);
                }

            }

            SetClientServiceForComboBox();
            ChangeEntryIsSelected();
        }

        private void SetClientServiceForComboBox() {
            var clientServices = (from b in db.ClientServices
                                  orderby b.name
                                  select new { Name = b.name, ID = b.ID })
                            .ToList();

            cbListServices.ItemsSource = clientServices;
            cbListServices.DisplayMemberPath = "Name";
            cbListServices.SelectedValuePath = "ID";
            cbListServices.SelectedValue = clientServices.FirstOrDefault();
        }

        #region enable/disable buttons (plus init's)
        private void ChangeEntryNotSelected() {
            entryStatus = EntryStatus.EntryNotSelected;
            btnCancel.IsEnabled = false;
            btnEdit.IsEnabled = false;
            btnNew.IsEnabled = true;
            btnSave.IsEnabled = false;
            btnDelete.IsEnabled = false;
            cbListServices.IsEnabled = true;
            EnableDisableUserInputConstrols(false);
        }

        private void ChangeEntryIsSelected() {

            entryStatus = EntryStatus.EntryIsSelected;
            btnCancel.IsEnabled = false;
            btnEdit.IsEnabled = true;
            btnNew.IsEnabled = true;
            btnSave.IsEnabled = false;
            btnDelete.IsEnabled = true;
            cbListServices.IsEnabled = true;
            EnableDisableUserInputConstrols(false);
        }

        private void ChangeEntryToEditMode() {
            MsgToUser("Edit Mode", 1);
            entryStatus = EntryStatus.EntryIsEdited;
            btnCancel.IsEnabled = true;
            btnEdit.IsEnabled = false;
            btnNew.IsEnabled = false;
            btnSave.IsEnabled = true;
            btnDelete.IsEnabled = false;
            cbListServices.IsEnabled = false;
            EnableDisableUserInputConstrols(true);
        }

        private void ChangeEntryToNewMode() {
            MsgToUser("Creating New Entry", 1);
            entryStatus = EntryStatus.EntryIsNew;
            btnCancel.IsEnabled = true;
            btnEdit.IsEnabled = false;
            btnNew.IsEnabled = false;
            btnSave.IsEnabled = true;
            btnDelete.IsEnabled = false;
            cbListServices.IsEnabled = false;
            EnableDisableUserInputConstrols(true);
        }

        private List<TextBox> GetAllTextBoxes() {
            var resultList = new List<TextBox>();
            foreach (var c in LogicalTreeHelper.GetChildren(GridClient)) {
                if (c.GetType() == typeof(TextBox)) {
                    resultList.Add((TextBox)c);
                }
            }
            return resultList;
        }

        private List<CheckBox> GetAllCheckBoxes() {
            var resultList = new List<CheckBox>();
            foreach (var c in LogicalTreeHelper.GetChildren(GridClient)) {
                if (c.GetType() == typeof(CheckBox)) {
                    resultList.Add((CheckBox)c);
                }
            }
            return resultList;
        }

        private void EnableDisableUserInputConstrols(bool enable) {
            foreach (var textBox in textBoxes) {
                textBox.IsEnabled = enable;
            }
            foreach (var checkBox in checkBoxes) {
                checkBox.IsEnabled = enable;
            }
        }

        #endregion


        private void cbListServices_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            ChangeEntryIsSelected();

            try {
                clientService = (from ao in db.ClientServices
                                 where ao.ID == (int)cbListServices.SelectedValue
                                 select ao).Single();
                clientServiceViewModel = new ClientServiceViewModel(clientService, btnSave);
                this.DataContext = clientServiceViewModel;
            } catch {
                // listener to combobox changes, after a aninimal owner has been permantly deleted
                ChangeEntryNotSelected();
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e) {
            ChangeEntryToEditMode();
        }

        private void btnNew_Click(object sender, RoutedEventArgs e) {
            ChangeEntryToNewMode();
            clientService = new ClientService();
            clientService.isEnabled = true;
            clientServiceViewModel = new ClientServiceViewModel(clientService, btnSave);
            this.DataContext = clientServiceViewModel;
            foreach (var tb in textBoxes) {
                tb.Text = "";
            }

        }

        private void btnDelete_Click(object sender, RoutedEventArgs e) {
            MessageBoxResult result = MessageBox.Show("Please confirm to delete " + clientService.name, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes) {

                // permanetly delete Service
                ChangeEntryNotSelected();
                MsgToUser("Permanently Deleted Record", 5);
                var validationsErrors = myService.DeleteClientService(clientService);
                Reset();

                if (validationsErrors != null) { // ERRORS EXIT (DATA WAS NOT COMMITTED TO DB)!
                    MsgToUser("Failed to Deleted Record", 5);
                }
            }
        }

        private void Reset() {
            clientServiceViewModel = null;
            this.DataContext = clientServiceViewModel;
            ChangeEntryNotSelected();
            SetClientServiceForComboBox();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) {
            MessageBoxResult result = MessageBox.Show("Do you wish to Cancel? Changes will be lost! ", "Cancel?", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes) {
                Reset();
                MsgToUser("Cancelled!", 5);
            }
        }

        private void MsgToUser(string message, int seconds) {

            lblMsgToUser.Background = System.Windows.Media.Brushes.Yellow;
            lblMsgToUser.Content = message;
            initTimer(seconds);

        }

        private DispatcherTimer updateTimer;

        private void initTimer(int seconds) {
            updateTimer = new DispatcherTimer(DispatcherPriority.Normal);
            updateTimer.Tick += new EventHandler(OnUpdateTimerTick);
            updateTimer.Interval = TimeSpan.FromSeconds(seconds);
            updateTimer.Start();
        }

        private void OnUpdateTimerTick(object sender, EventArgs e) {
            lblMsgToUser.Background = System.Windows.Media.Brushes.Transparent;
            lblMsgToUser.Content = " ";
        }


    }
}
