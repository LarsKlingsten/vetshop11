﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using VetShopDB2;
using VetShopWPF.ModelView;

using System.Collections.Generic;
using System.Windows.Threading;
using System;

namespace VetShopWPF.Tabs {


    /// <summary>
    /// Tab/windows that allows for CRUD operations on AnimalOwner table
    /// </summary>
    public partial class CreateUser : UserControl {

        private Dao myService = Dao.GetInstance();
        private VetShopDBEntities db = Dao.GetInstance().GetEntities();
        private AnimalOwnerViewModel animalOwnerViewModel;
        private AnimalOwner animalOwner;
        private EntryStatus entryStatus;
        private List<TextBox> textBoxes;
        private List<CheckBox> checkBoxes;

        public CreateUser() {
            InitializeComponent();
            textBoxes = GetAllTextBoxes();
            checkBoxes = GetAllCheckBoxes();
            ChangeEntryNotSelected();
            SetAnimalOwnersForComboBox();
            this.DataContext = animalOwnerViewModel;
        }

        private AnimalOwner ConvertAnimalViewModelToAnimalOwner() {

            if (entryStatus == EntryStatus.EntryIsNew) { // for new entries 
                animalOwner = new AnimalOwner();
            }

            animalOwner.name = animalOwnerViewModel.OwnerName;
            animalOwner.address = animalOwnerViewModel.Address;
            animalOwner.phoneNumber = animalOwnerViewModel.PhoneNumber;
            animalOwner.postNumber = animalOwnerViewModel.PostNumber;
            animalOwner.emailAddress = animalOwnerViewModel.EmailAddress;
            animalOwner.byNavn = animalOwnerViewModel.ByNavn;
            animalOwner.loginID = animalOwnerViewModel.LoginID;
            animalOwner.loginPassword = animalOwnerViewModel.LoginPassword;
            animalOwner.isStillAClient = animalOwnerViewModel.IsStillAClient;
            animalOwner.isWebAccessEnabled = animalOwnerViewModel.IsWebAccessEnabled;

            return animalOwner;
        }

        // Save the object
        private void btnSave_Click(object sender, RoutedEventArgs e) {
            AnimalOwner animalOwner = ConvertAnimalViewModelToAnimalOwner();

            // popup window for errors 
            //- which should only incur on database being unavailable
            // considering that each of the textboxes are validated.

            if (entryStatus == EntryStatus.EntryIsNew) {
                var validationsErrors = myService.AddAnimalOwner(animalOwner);


                if (validationsErrors != null) { // ERRORS EXIT (DATA WAS NOT COMMITTED TO DB)!
                    MsgToUser("Could not make new record", 3);
                } else {
                    MsgToUser("Created New Record", 3);
                }
            }

            if (entryStatus == EntryStatus.EntryIsEdited) {
                var validationsErrors = myService.UpdateAnimalOwner(animalOwner);

                if (validationsErrors != null) { // ERRORS EXIT (DATA WAS NOT COMMITTED TO DB)!
                    MsgToUser("Could not make new record", 3);
                } else {
                    MsgToUser("Updated Record", 3);
                }

            }

            SetAnimalOwnersForComboBox();
            ChangeEntryIsSelected();
        }

        private void SetAnimalOwnersForComboBox() {
            var animalOwners = (from b in db.AnimalOwners
                                orderby b.name
                                select new { Name = b.name, ID = b.ID })
                            .ToList();

            cbListClients.ItemsSource = animalOwners;
            cbListClients.DisplayMemberPath = "Name";
            cbListClients.SelectedValuePath = "ID";
            cbListClients.SelectedValue = animalOwners.FirstOrDefault();
        }

        #region enable/disable buttons (plus init's)
        private void ChangeEntryNotSelected() {
            entryStatus = EntryStatus.EntryNotSelected;
            btnCancel.IsEnabled = false;
            btnEdit.IsEnabled = false;
            btnNew.IsEnabled = true;
            btnSave.IsEnabled = false;
            btnDelete.IsEnabled = false;
            cbListClients.IsEnabled = true;
            EnableDisableUserInputConstrols(false);
        }

        private void ChangeEntryIsSelected() {
            
            entryStatus = EntryStatus.EntryIsSelected;
            btnCancel.IsEnabled = false;
            btnEdit.IsEnabled = true;
            btnNew.IsEnabled = true;
            btnSave.IsEnabled = false;
            btnDelete.IsEnabled = true;
            cbListClients.IsEnabled = true;
            EnableDisableUserInputConstrols(false);
        }

        private void ChangeEntryToEditMode() {
            MsgToUser("Edit Mode", 1);
            entryStatus = EntryStatus.EntryIsEdited;
            btnCancel.IsEnabled = true;
            btnEdit.IsEnabled = false;
            btnNew.IsEnabled = false;
            btnSave.IsEnabled = true;
            btnDelete.IsEnabled = false;
            cbListClients.IsEnabled = false;
            EnableDisableUserInputConstrols(true);
        }

        private void ChangeEntryToNewMode() {
            MsgToUser("Creating New Entry", 1);
            entryStatus = EntryStatus.EntryIsNew;
            btnCancel.IsEnabled = true;
            btnEdit.IsEnabled = false;
            btnNew.IsEnabled = false;
            btnSave.IsEnabled = true;
            btnDelete.IsEnabled = false;
            cbListClients.IsEnabled = false;
            EnableDisableUserInputConstrols(true);
        }

        private List<TextBox> GetAllTextBoxes() {
            var resultList = new List<TextBox>();
            foreach (var c in LogicalTreeHelper.GetChildren(GridClient)) {
                if (c.GetType() == typeof(TextBox)) {
                    resultList.Add((TextBox)c);
                }
            }
            return resultList;
        }

        private List<CheckBox> GetAllCheckBoxes() {
            var resultList = new List<CheckBox>();
            foreach (var c in LogicalTreeHelper.GetChildren(GridClient)) {
                if (c.GetType() == typeof(CheckBox)) {
                    resultList.Add((CheckBox)c);
                }
            }
            return resultList;
        }

        private void EnableDisableUserInputConstrols(bool enable) {
            foreach (var textBox in textBoxes) {
                textBox.IsEnabled = enable;
            }
            foreach (var checkBox in checkBoxes) {
                checkBox.IsEnabled = enable;
            }
        }

        #endregion


        private void cbListClients_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            ChangeEntryIsSelected();

            try {
                animalOwner = (from ao in db.AnimalOwners
                               where ao.ID == (int)cbListClients.SelectedValue
                               select ao).Single();
                animalOwnerViewModel = new AnimalOwnerViewModel(animalOwner, btnSave);
                this.DataContext = animalOwnerViewModel;
            } catch {
                // listener to combobox changes, after a aninimal owner has been permantly deleted
                ChangeEntryNotSelected();
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e) {
            ChangeEntryToEditMode();
           
        }

        private void btnNew_Click(object sender, RoutedEventArgs e) {
            ChangeEntryToNewMode();
            animalOwner = new AnimalOwner();
            animalOwner.isStillAClient = true;
            animalOwnerViewModel = new AnimalOwnerViewModel(animalOwner, btnSave);
            this.DataContext = animalOwnerViewModel;
            foreach (var tb in textBoxes) {
                tb.Text = "";
            }

        }

        private void btnDelete_Click(object sender, RoutedEventArgs e) {
            MessageBoxResult result = MessageBox.Show("Please confirm to delete " + animalOwner.name, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes) {

                if (animalOwner.Animals.Count() > 0) {  // do not permanetly delete AnimalOwner that still have Animals
                    animalOwner.isStillAClient = false;
                    var validationsErrors = myService.UpdateAnimalOwner(animalOwner);
                    MsgToUser("Deleted record (Recoverable)", 5);
                    ChangeEntryIsSelected();
                } else {                                // permanetly delete AnimalOwner (that has no Animals)
                    ChangeEntryNotSelected();
                    MsgToUser("Permanently Deleted Record", 5);
                    var validationsErrors = myService.DeleteAnimalOwner(animalOwner);
                    Reset();


                    if (validationsErrors != null) { // ERRORS EXIT (DATA WAS NOT COMMITTED TO DB)!
                        MsgToUser("Error! -Could note deleted Record", 5);
                    }
                }
            }
        }

        private void Reset() {
            animalOwnerViewModel = null;
            this.DataContext = animalOwnerViewModel;
            ChangeEntryNotSelected();
            SetAnimalOwnersForComboBox();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) {
            MessageBoxResult result = MessageBox.Show("Do you wish to Cancel? Changes will be lost! ", "Cancel?", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes) {
                Reset();
                MsgToUser("Cancelled!", 5);
            }
        }

        private void MsgToUser(string message, int seconds) {

            lblMsgToUser.Background = System.Windows.Media.Brushes.Yellow;
            lblMsgToUser.Content = message;
            initTimer(seconds);

        }

        private DispatcherTimer updateTimer;

        private void initTimer(int seconds) {
            updateTimer = new DispatcherTimer(DispatcherPriority.Normal);
            updateTimer.Tick += new EventHandler(OnUpdateTimerTick);
            updateTimer.Interval = TimeSpan.FromSeconds(seconds);
            updateTimer.Start();
        }

        private void OnUpdateTimerTick(object sender, EventArgs e) {
            lblMsgToUser.Background = System.Windows.Media.Brushes.Transparent;
            lblMsgToUser.Content = " ";
        }


    }
}
