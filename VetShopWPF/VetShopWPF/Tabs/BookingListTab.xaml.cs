﻿using System;
using System.Linq;
using System.Windows.Controls;

using VetShopDB2;
using VetShopWPF.ModelView;
 
using System.Collections.ObjectModel;

namespace VetShopWPF.Tabs {
    public partial class BookingListTab : UserControl {
        Dao myService = Dao.GetInstance();
        VetShopDBEntities db = Dao.GetInstance().GetEntities();
        ObservableCollection<BookingViewModel> bookingsObsList;

        public BookingListTab() {
            InitializeComponent();
            bookingsObsList = new ObservableCollection<BookingViewModel>();
            dataGridBookings.ItemsSource = bookingsObsList;
            SetUniquneBookingDatesForComboBox();
            SetAnimalForComboBox();
        }

        private void cbSelectDate_SelectionChanged(object sender, SelectionChangedEventArgs e) {
      
            var myDate = ((DateTime)cbSelectDate.SelectedValue).Date;
            bookingsObsList.Clear();
 
            var bookings = db.Bookings.ToList().Where(i => i.consultationStart.Value.Date == myDate).ToList();
            var bookingModels = (from b in bookings 
                                 orderby b.consultationStart
                                 select new BookingViewModel {
                                     ID = b.ID,
                                     ConsultationStart = (DateTime)b.consultationStart,
                                     Owner = b.Animal.AnimalOwner.name,
                                     Animal = b.Animal.name,
                                     Kind = b.Animal.Specie.animalType
                                 }).ToList();

            foreach (var b in bookingModels) {
                bookingsObsList.Add(b);
            }
        }

        private void SetUniquneBookingDatesForComboBox() {
            var bookings = (from b in db.Bookings
                            orderby b.consultationStart
                            select new { CStart = b.consultationStart })
                            .ToList().GroupBy(u => u.CStart.Value.Date).Distinct();
            cbSelectDate.ItemsSource = bookings;
            cbSelectDate.DisplayMemberPath = "CStart";
            cbSelectDate.SelectedValuePath = "CStart";
            cbSelectDate.SelectedValue = bookings.FirstOrDefault();
        }

        private void SetAnimalForComboBox() {
            var bookings = (from b in db.Animals
                            orderby b.name
                            select new { AnimalName = b.name + " ("+b.AnimalOwner.name +")", ID = b.ID })
                            .ToList();
            cbSelectAnimal.ItemsSource = bookings;
            cbSelectAnimal.DisplayMemberPath = "AnimalName";
            cbSelectAnimal.SelectedValuePath = "ID";
            cbSelectAnimal.SelectedValue = bookings.FirstOrDefault();
        }

        private void cbSelectAnimal_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var animalID = (int) cbSelectAnimal.SelectedValue  ;
            bookingsObsList.Clear();
                
            var bookingmodels = (from b in db.Bookings
                                 where b.Animal.ID == animalID
                                 select new BookingViewModel {
                                     ID = b.ID,
                                     ConsultationStart = (DateTime)b.consultationStart,
                                     Owner = b.Animal.AnimalOwner.name,
                                     Animal = b.Animal.name,
                                     Kind = b.Animal.Specie.animalType
                                 }).ToList();

            foreach (var b in bookingmodels) { 
                bookingsObsList.Add(b);
            }
        }
    }
}