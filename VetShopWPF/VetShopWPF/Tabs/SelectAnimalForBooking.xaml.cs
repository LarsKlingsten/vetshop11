﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using VetShopDB2;

namespace VetShopWPF.Tabs {
    /// <summary>
    /// Interaction logic for SelectAnimalForBooking.xaml
    /// </summary>
    public partial class SelectAnimalForBooking : Window {


        private Dao myDao = Dao.GetInstance();
        private VetShopDBEntities db = Dao.GetInstance().GetEntities();
        public int SelectedAnimalID;

        public SelectAnimalForBooking() {
            InitializeComponent();
            UpdateCbAnimals();

        }


        private void UpdateCbAnimals() {
            var animals = (from b in db.Animals
                           orderby b.name
                           select new { Name = b.name + " (" + b.AnimalOwner.name + ")", ID = b.ID })
                            .ToList();

            cbSelectAnimal.ItemsSource = animals;
            cbSelectAnimal.DisplayMemberPath = "Name";
            cbSelectAnimal.SelectedValuePath = "ID";
            cbSelectAnimal.SelectedValue = animals.FirstOrDefault();
        }

        private void cbSelectAnimal_SelectionChanged(object sender, SelectionChangedEventArgs e) {

            try {
                SelectedAnimalID = (int)cbSelectAnimal.SelectedValue;
            } catch { }
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            this.Close();
        }

    }
}
