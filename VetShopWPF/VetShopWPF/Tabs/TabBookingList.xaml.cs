﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using MySnippets;
using VetShipDB;
using VetShopWPF.ModelView;
using VetShopWPF.Services;
using System.Collections.ObjectModel; 

namespace VetShopWPF.Tabs {
    /// <summary>
    /// Interaction logic for TabBookingList.xaml
    /// </summary>
    public partial class TabBookingList : UserControl {

        Service myService = Service.GetInstance();
        VetShopDBEntities db = Service.GetInstance().GetEntities();      
  
         ObservableCollection<BookingViewModel> bookingsObsList;
          


        public TabBookingList() {
            InitializeComponent();

            bookingsObsList = new ObservableCollection<BookingViewModel>();
            dataGridBookings.ItemsSource = bookingsObsList;

            SetUniquneBookingDatesForComboBox();



           
        }



        private void cbSelectDate_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (cbSelectDate.SelectedValue == null) { "no selection".ToConsoleWithTime(); return; }
            var myDate = ((DateTime)cbSelectDate.SelectedValue).Date;
            bookingsObsList.Clear();
            foreach (var b in myService.GetBookings(myDate)) {
                bookingsObsList.Add(b);
            }
        }
            
        private void SetUniquneBookingDatesForComboBox() {
            var bookings = (from b in db.Bookings
                            orderby b.updated
                            select new { Updated = b.updated })
                            .ToList().GroupBy(u => u.Updated.Date).Distinct();
            cbSelectDate.ItemsSource = bookings;
            cbSelectDate.DisplayMemberPath = "Updated";
            cbSelectDate.SelectedValuePath = "Updated";
            cbSelectDate.SelectedValue = bookings.FirstOrDefault();
        }

        }
    }
